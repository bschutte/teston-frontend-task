const WebpackNotifierPlugin = require("webpack-notifier");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const webpack = require("webpack");
const path = require("path");

module.exports = {
    mode: "production",
    context: path.resolve(__dirname, "src"),
    entry: ["./js/frontend-task.js", "./scss/style.scss"],
    output: {
        path: path.resolve(__dirname, "dist", "assets"),
        filename: "js/app.bundle.js",
        publicPath: "/assets/",
    },
    plugins: [
        new CleanWebpackPlugin(), //cleans the dist folder
        new CopyWebpackPlugin([
            {
                from: './fonts',
                to: 'fonts'
            }
        ]),
        new WebpackNotifierPlugin({title: "Webpack notifier"}),
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "babel-loader",
                        options: {
                            cacheDirectory: true
                        }
                    }
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "css/[name].css",
                        }
                    },
                    {
                        loader: "extract-loader"
                    },
                    {
                        loader: "css-loader?-url"
                    },
                    {
                        loader: "postcss-loader"
                    },
                    {
                        loader: "sass-loader"
                    }
                ]
            }
        ]
    },
    devtool: false
};