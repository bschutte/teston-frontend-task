Teston frontend task
=================================

This repository contains the code for the implementation of the frontend task from teston.

## Setup
**Install packages**

Install required packages with either yarn or npm by running:

```
yarn install
```
```
npm install
```

**Start the built-in web server**

Run the application:

```
yarn start
```
```
npm run start
```

Now check out the application at `http://localhost:3000`
> ⚠️ The webserver runs on port 3000. If this port is taken by another process, the script will throw an error. Update the `devServer` settings in webpack.config.js to configure a different port.