import React from "react";
import PropTypes from "prop-types";

const Navigation = props => {
    const { activeSlideIndex, showNavigation, slides, moveToNextSlide, forms } = props;

    const styles = {
        opacity: 1
    };

    // get the slide indexes on which navigation needs to be shown
    const paginationItems = slides.filter(slide => {
        return slide.pagination;
    });

    const showOnIndexes = paginationItems.map(slide => {
        return slide.index;
    });

    const stylesItemActive = (() => {
        const firstIndexToShowOn = Math.min(...showOnIndexes);
        const lastIndexToShowOn = Math.max(...showOnIndexes);

        // if the index of the current active slide is not an index
        // that needs to show the active item, don't return styles
        if (activeSlideIndex <= firstIndexToShowOn) { return null; }

        // calculate the position of the active item based on the active slide
        // slide 0 = 0%, slide 1 = 0%, slide 2 = 100%, slide 3 = 200% etc. etc.
        let translatePercentage = (activeSlideIndex > lastIndexToShowOn) ? lastIndexToShowOn : activeSlideIndex;
        translatePercentage = (translatePercentage - 1) * 100;

        return {
            transform: `translateX(${translatePercentage}%)`
        };
    })();

    const handlePaginationClick = event => {
        event.preventDefault();

        const { target } = event;
        const href = target.getAttribute("href");

        const clickedIndex = (() => {
            const slideIndex = slides.findIndex(slide => {
                return slide.href === href;
            });

            return slides[slideIndex].index;
        })();

        if (clickedIndex > activeSlideIndex) {
            forms[0].current.querySelector("[type=submit]").click();
        } else {
            moveToNextSlide(href);
        }
    };

    return (
        <nav
            className="navigation"
            style={showNavigation ? styles : null }
        >
            <ul>
                <li
                    className="navigation__item navigation__item--active"
                    style={stylesItemActive}
                >
                    <span></span>
                </li>
                {
                    paginationItems.map(paginationItem => {
                        const isActive = activeSlideIndex === paginationItem.index;

                        return (
                            <li
                                key={paginationItem.index}
                                className="navigation__item"
                            >
                                {
                                    isActive
                                        ? <span>{ paginationItem.label }</span>
                                        : <a
                                            href={ paginationItem.href }
                                            title={ paginationItem.title }
                                            onClick={handlePaginationClick}
                                        >
                                            { paginationItem.label }
                                        </a>
                                }
                            </li>
                        );
                    })
                }
            </ul>
        </nav>
    );
};

export default Navigation;

Navigation.propTypes = {
    activeSlideIndex: PropTypes.number.isRequired,
    showNavigation: PropTypes.bool.isRequired,
    slides: PropTypes.arrayOf(
        PropTypes.shape({
            index: PropTypes.number,
            title: PropTypes.string,
            label: PropTypes.string,
            pagination: PropTypes.bool,
            href: PropTypes.string
        })
    ).isRequired,
    moveToNextSlide: PropTypes.func.isRequired,
    forms: PropTypes.array.isRequired
};
