import React from "react";
import PropTypes from "prop-types";

const Slide = props => {
    const returnClassName = () => {
        const { index, activeSlideIndex, className = [] } = props;
        const isActive = activeSlideIndex === index;

        className.unshift("slide");

        if (isActive) {
            className.push("slide--active");
        }

        return className.join(" ");
    };

    return (
        <section className={returnClassName()}>
            { props.children }
        </section>
    );
};

export default Slide;

Slide.propTypes = {
    index: PropTypes.number.isRequired,
    activeSlideIndex: PropTypes.number.isRequired,
    children: PropTypes.node.isRequired,
    className: PropTypes.arrayOf(PropTypes.string)
};
