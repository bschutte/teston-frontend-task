import React from "react";
import PropTypes from "prop-types";
import Slide from "./Slide";

const Intro = props => {
    const { index, activeSlideIndex, moveToNextSlide } = props;

    return (
        <Slide
            index={index}
            activeSlideIndex={activeSlideIndex}
            className={["slide--intro"]}
        >
            <button
                type="button"
                className="button button--cta button--pink button--large"
                onClick={() => { moveToNextSlide("lets-get-started"); }}
            >
                Get started
            </button>
        </Slide>
    );
};

export default Intro;

Intro.propTypes = {
    index: PropTypes.number.isRequired,
    activeSlideIndex: PropTypes.number.isRequired,
    moveToNextSlide: PropTypes.func.isRequired
};
