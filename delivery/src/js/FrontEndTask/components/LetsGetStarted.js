import React from "react";
import Slide from "./Slide";
import SlideFooter from "./SlideFooter";
import RadioGroup from "./RadioGroup";
import PropTypes from "prop-types";

class LetsGetStarted extends React.Component {
    constructor (props) {
        super(props);

        const { forms } = props;

        forms.push(React.createRef());

        this.fields = ["favouriteToe", "stance"];

        this.options = {
            [this.fields[0]]: [
                {
                    label: "Big",
                    value: "big"

                }, {
                    label: "Pointer",
                    value: "pointer"

                }, {
                    label: "Middle",
                    value: "middle"
                }, {
                    label: "Ring",
                    value: "ring"
                }, {
                    label: "Baby",
                    value: "baby"
                }
            ],
            [this.fields[1]]: [
                {
                    label: "Left foot",
                    value: "left foot"

                }, {
                    label: "Right foot",
                    value: "right foot"

                }, {
                    label: "Both feet",
                    value: "both feet"
                }
            ]
        };
    }

    footerIsActive = () => {
        const { favouriteToe, stance } = this.props;

        return favouriteToe.length !== 0 && stance.length !== 0;
    };

    handleSubmit = event => {
        const { moveToNextSlide } = this.props;

        event.preventDefault();
        moveToNextSlide("one-more-thing");
    };

    render () {
        const { index, activeSlideIndex, favouriteToe, stance, forms } = this.props;
        const footerIsActive = this.footerIsActive();

        return (
            <Slide
                index={index}
                activeSlideIndex={activeSlideIndex}
                className={["slide--lets-get-started"]}
            >
                <form
                    ref={forms[0]}
                    onSubmit={this.handleSubmit}
                >
                    <header>
                        <h2>Let's get started!</h2>
                        <p>But first, we need to get to know each other. Just answer these quick questions!</p>
                    </header>

                    <fieldset>
                        <legend>What's your favourite toe?</legend>

                        <div className={`form-row form-row--${this.options[this.fields[0]].length}-items`}>
                            <div className={`form-row__field form-row__field--${this.fields[0]}`}>
                                <RadioGroup
                                    name={this.fields[0]}
                                    options={this.options[this.fields[0]]}
                                    stateValue={favouriteToe}
                                    {...this.props}
                                />
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>What's your stance?</legend>

                        <div className={`form-row form-row--${this.options[this.fields[1]].length}-items`}>
                            <div className={`form-row__field form-row__field--${this.fields[1]}`}>
                                <RadioGroup
                                    name={this.fields[1]}
                                    options={this.options[this.fields[1]]}
                                    stateValue={stance}
                                    {...this.props}
                                />
                            </div>
                        </div>
                    </fieldset>

                    <SlideFooter isActive={footerIsActive}>
                        <p>Awesome. Let's move on.</p>
                        <button
                            type="submit"
                            className={`button button--cta button--pink${!footerIsActive ? " disabled" : ""}`}
                        >
                            Continue ›
                        </button>
                    </SlideFooter>
                </form>
            </Slide>
        );
    };
}

export default LetsGetStarted;

LetsGetStarted.propTypes = {
    index: PropTypes.number.isRequired,
    activeSlideIndex: PropTypes.number.isRequired,
    favouriteToe: PropTypes.string.isRequired,
    stance: PropTypes.string.isRequired,
    moveToNextSlide: PropTypes.func.isRequired,
    forms: PropTypes.array.isRequired
};
