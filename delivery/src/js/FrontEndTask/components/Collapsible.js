import React from "react";
import PropTypes from "prop-types";

import { returnTallestChild } from "../../helper";

class Collapsible extends React.Component {
    constructor (props) {
        super(props);

        this.collapsible = React.createRef();
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        return (
            this.props.collapsed !== nextProps.collapsed ||
            this.props.contemptExplanation !== nextProps.contemptExplanation
        );
    }

    render () {
        const { collapsed } = this.props;
        const styles = (() => {
            let height = 0;

            if (!collapsed) {
                height = returnTallestChild(this.collapsible.current.children);
            }

            height += "px";

            return {
                height
            };
        })();

        return (
            <div
                ref={this.collapsible}
                className={`collapsible${collapsed ? " collapsible--collapsed" : ""}`}
                style={styles}
            >
                { this.props.children }
            </div>
        );
    };
}

export default Collapsible;

Collapsible.propTypes = {
    children: PropTypes.node.isRequired,
    collapsed: PropTypes.bool.isRequired,
    contemptExplanation: PropTypes.string.isRequired
};
