const WebpackNotifierPlugin = require("webpack-notifier");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");
const path = require("path");

module.exports = {
    mode: "development",
    context: path.resolve(__dirname, "src"),
    entry: {
        app: ["@babel/polyfill", "./js/frontend-task.js"]
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "[name].bundle.js"
    },
    plugins: [
        new HtmlWebpackPlugin({    
            template: "./html/frontend-task.ejs"
        }),
        new WebpackNotifierPlugin({title: "Webpack notifier"}),
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "babel-loader",
                        options: {
                            cacheDirectory: true
                        }
                    }
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    "style-loader", // creates style nodes from JS strings
                    "css-loader", // translates CSS into CommonJS
                    "sass-loader" // compiles Sass to CSS, using Node Sass by default
                ]
            }
        ]
    },
    devServer: {
        port: 3000,   //Tell dev-server which port to run
        open: true,   // to open the local server in browser
        contentBase: path.resolve(__dirname, "dist") //serve from "dist" folder
    },
    devtool: "inline-source-map",
};