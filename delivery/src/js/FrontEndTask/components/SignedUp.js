import React from "react";
import Slide from "./Slide";
import PropTypes from "prop-types";

const SignedUp = props => {
    const { index, activeSlideIndex } = props;

    return (
        <Slide
            index={index}
            activeSlideIndex={activeSlideIndex}
            className={["slide--signed-up"]}
        >
            <svg className="icon icon--checkmark" viewBox="0 0 95 95">
                <use xlinkHref="#icon--checkmark" />
            </svg>
            <p>Signed up!</p>
        </Slide>
    );
};

export default SignedUp;

SignedUp.propTypes = {
    index: PropTypes.number.isRequired,
    activeSlideIndex: PropTypes.number.isRequired
};
