"use strict";

import React from "react";
import { render } from "react-dom";
import FrontEndTask from "./FrontEndTask/FrontEndTask";

render(
    <FrontEndTask />,
    document.querySelector("#react-app")
);
