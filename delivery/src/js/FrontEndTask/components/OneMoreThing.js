import React from "react";
import PropTypes from "prop-types";

import Slide from "./Slide";
import RadioGroup from "./RadioGroup";
import SlideFooter from "./SlideFooter";
import Collapsible from "./Collapsible";

const OneMoreThing = props => {
    const { index, activeSlideIndex, moveToNextSlide, contempt, contemptExplanation, preferredSubject, updateState } = props;

    const footerIsActive = () => {
        return contempt.length !== 0 && preferredSubject.length !== 0;
    };

    const fields = ["contempt", "contemptExplanation", "preferredSubject"];

    const handleContemptChange = changeEvent => {
        updateState(fields[0], changeEvent.target.value);
    };

    const handleContemptExplanationChange = changeEvent => {
        updateState(fields[1], changeEvent.target.value);
    };

    const handleSubmit = event => {
        event.preventDefault();
        moveToNextSlide("signed-up");
    };

    const options = {
        [fields[2]]: [
            {
                label: "Normal",
                value: "normal"

            }, {
                label: "A little weird",
                value: "a little weird"

            }, {
                label: "This one",
                value: "this one"
            }
        ]
    };

    const contemptSliderWidth = () => {
        const minWidthOffset = 8;
        const maxWidthOffset = 100 - minWidthOffset;
        let width;

        if (contempt < minWidthOffset) {
            width = `calc(${contempt}% + 10px)`;
        } else if (contempt >= maxWidthOffset) {
            width = `calc(${contempt}% - 10px)`;
        } else {
            width = `${contempt}%`;
        }

        return {
            width
        };
    };

    return (
        <Slide
            index={index}
            activeSlideIndex={activeSlideIndex}
            className={["slide--one-more-thing"]}
        >
            <header>
                <h2>One more thing..</h2>
                <p>Just another small detail. Don't worry about it.
                You can trust us, we're a form. It's for science.</p>
            </header>

            <form onSubmit={handleSubmit}>
                <fieldset>
                    <legend>Form contempt level</legend>

                    <div className="form-row">
                        <div className="form-row__label">
                            <label htmlFor={`form_${fields[0]}`}>How displeased were you with this form?</label>
                        </div>
                        <div className={`form-row__field form-row__field--${fields[0]}`}>
                            <div className={`${fields[0]}-slider`}>
                                <div
                                    className={`${fields[0]}-slider__value`}
                                    style={contemptSliderWidth()}
                                ></div>
                            </div>
                            <input
                                type="range"
                                name={`form[${fields[0]}]`}
                                id={`form_${fields[0]}`}
                                min="0"
                                max="100"
                                value={contempt}
                                onChange={handleContemptChange}
                                required
                            />
                        </div>
                    </div>

                    <Collapsible
                        collapsed={contempt === "0"}
                        contemptExplanation={contemptExplanation}
                    >
                        <div className="form-row">
                            <div className="form-row__label">
                                <label htmlFor={`form_${fields[1]}`}>Please explain your answer</label>
                            </div>
                            <div className={`form-row__field form-row__field--${fields[1]}`}>
                                <textarea
                                    cols="50"
                                    rows="3"
                                    name={`form[${fields[1]}]`}
                                    id={`form_${fields[1]}`}
                                    placeholder="Please explain your answer"
                                    value={contemptExplanation}
                                    onChange={handleContemptExplanationChange}
                                >
                                </textarea>
                            </div>
                        </div>
                    </Collapsible>
                </fieldset>

                <fieldset>
                    <legend>Preferred form subject</legend>

                    <div className={`form-row form-row--${options[fields[2]].length}-items`}>
                        <div className="form-row__label">
                            <p>What type of form would you prefer?</p>
                        </div>

                        <div className={`form-row__field form-row__field--${fields[2]}`}>
                            <RadioGroup
                                name={fields[2]}
                                options={options[fields[2]]}
                                stateValue={preferredSubject}
                                {...props}
                            />
                        </div>
                    </div>
                </fieldset>

                <SlideFooter isActive={footerIsActive()}>
                    <p>All right, that's it. We're good!</p>
                    <button
                        type="submit"
                        className="button button--cta button--pink"
                        disabled={!footerIsActive()}
                    >
                        Sign me up!
                    </button>
                </SlideFooter>
            </form>
        </Slide>
    );
};

export default OneMoreThing;

OneMoreThing.propTypes = {
    index: PropTypes.number.isRequired,
    activeSlideIndex: PropTypes.number.isRequired,
    contempt: PropTypes.string.isRequired,
    contemptExplanation: PropTypes.string.isRequired,
    preferredSubject: PropTypes.string.isRequired,
    moveToNextSlide: PropTypes.func.isRequired,
    updateState: PropTypes.func.isRequired
};
