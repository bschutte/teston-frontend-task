import React from "react";
import PropTypes from "prop-types";

const SlideFooter = props => {
    const { isActive } = props;

    return (
        <footer className={`slide__footer${isActive ? " slide__footer--active" : ""}`}>
            { props.children }
        </footer>
    );
};

export default SlideFooter;

SlideFooter.propTypes = {
    isActive: PropTypes.bool.isRequired,
    children: PropTypes.node.isRequired
};
