import React from "react";
import PropTypes from "prop-types";

const RadioGroup = props => {
    const { name, options, stateValue, updateState } = props;

    const handleOptionChange = changeEvent => {
        updateState(name, changeEvent.target.value);
    };

    return (
        options.map((option, index) => {
            return (
                <React.Fragment key={option.value}>
                    <input
                        type="radio"
                        name={`form[${name}]`}
                        id={`form_${name}_${index}`}
                        value={option.value}
                        checked={option.value === stateValue}
                        onChange={handleOptionChange}
                        required
                    />
                    <label
                        htmlFor={`form_${name}_${index}`}
                        className="button button--small"
                    >{ option.label }</label>
                </React.Fragment>
            );
        })
    );
};

export default RadioGroup;

RadioGroup.propTypes = {
    name: PropTypes.string.isRequired,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string,
            value: PropTypes.string
        })
    ).isRequired,
    stateValue: PropTypes.any.isRequired,
    updateState: PropTypes.func.isRequired
};
