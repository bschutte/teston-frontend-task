const returnTallestChild = children => {
    let tallestChildHeight = 0;

    [...children].forEach(child => {
        if (child.offsetHeight > tallestChildHeight) {
            tallestChildHeight = child.offsetHeight;
        }
    });

    return tallestChildHeight;
};

export { returnTallestChild };
