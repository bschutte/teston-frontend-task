import React from "react";

import Intro from "./components/Intro";
import LetsGetStarted from "./components/LetsGetStarted";
import OneMoreThing from "./components/OneMoreThing";
import SignedUp from "./components/SignedUp";
import Navigation from "./components/Navigation";

class FrontEndTask extends React.Component {
    constructor (props) {
        super(props);

        this.slideOuter = React.createRef();

        this.slides = [{
            index: 0,
            pagination: false,
            href: null
        }, {
            index: 1,
            title: "Let's get started",
            label: "Let's get started",
            pagination: true,
            href: "lets-get-started"
        }, {
            index: 2,
            title: "One more thing..",
            label: "One more thing..",
            pagination: true,
            href: "one-more-thing"
        }, {
            index: 3,
            href: "signed-up",
            pagination: false
        }];

        this.forms = [];

        this.state = {
            appIsLoading: true,
            activeSlideIndex: 0,
            showNavigation: false,

            favouriteToe: "",
            stance: "",
            contempt: "0",
            contemptExplanation: "",
            preferredSubject: ""
        };
    }

    componentDidMount = () => {
        window.addEventListener("load", event => {
            this.setState({
                appIsLoading: false
            }, () => {
                document.body.classList.add("react-app-loaded");
            });
        });

        window.addEventListener("popstate", event => {
            this.slide(event.state);
        });
    };

    slide = href => {
        const slide = (() => {
            const index = this.slides.findIndex(slide => {
                return slide.href === href;
            });

            return this.slides[index];
        })();

        this.setState({
            activeSlideIndex: slide.index,
            showNavigation: slide.pagination
        });
    };

    updateState = (key, value) => {
        const state = {
            [key]: value
        };

        this.setState(state);
    };

    moveToNextSlide = event => {
        let href = event;

        if (typeof event === "object") {
            event.preventDefault();
            href = event.target.getAttribute("href");
        }

        history.pushState(href, null, href);

        this.slide(href);
    }

    render () {
        const { activeSlideIndex, showNavigation } = this.state;

        return (
            <>
                <main
                    ref={this.slideOuter}
                    className="slide-outer"
                >
                    <Intro
                        index={0}
                        activeSlideIndex={activeSlideIndex}
                        updateState={this.updateState}
                        moveToNextSlide={this.moveToNextSlide}
                        {...this.state}
                    />

                    <LetsGetStarted
                        index={1}
                        activeSlideIndex={activeSlideIndex}
                        updateState={this.updateState}
                        moveToNextSlide={this.moveToNextSlide}
                        forms={this.forms}
                        {...this.state}
                    />

                    <OneMoreThing
                        index={2}
                        activeSlideIndex={activeSlideIndex}
                        updateState={this.updateState}
                        moveToNextSlide={this.moveToNextSlide}
                        {...this.state}
                    />

                    <SignedUp
                        index={3}
                        activeSlideIndex={activeSlideIndex}
                    />
                </main>

                <Navigation
                    activeSlideIndex={activeSlideIndex}
                    showNavigation={showNavigation}
                    moveToNextSlide={this.moveToNextSlide}
                    slides={this.slides}
                    forms={this.forms}
                />
            </>
        );
    };
}

export default FrontEndTask;
